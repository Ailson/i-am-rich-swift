# I Am Rich - Swift

First project made in Swift language to learn to develop IOS apps.

# Motivation

This is the very simple app made to learn some fist overview on how to use the XCode tool, create a simple UI adding some elements and styling then.

# Screenshot

![Screenshot](screenshot/screenshot.png)

# Instalation (IOS - only)

To install this app, you can clone this repo, open it on XCode and click on the "Build and Run" button

# License

MIT Ailson Freire


